
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var spawn = require('child_process').spawn

var app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

app.get('/alice', function(req, res) {
  res.render('alice', { title: 'Alice' });
});

app.get('/dan', function(req, res) { res.render('bob', { title: 'Dan' }); });


app.post('/alicerun', function(req, res) {
	var ip = req.body.ip;
	var inpp = req.body.inpp;
	console.log("======run alice=======" + inpp);
	console.log("     " + ip);
	io.sockets.emit('news', { f: 'Starting Alice with Dan@ '+ip+' on input evlgr' } ); 

	alice = spawn('mpirun', ['-n',16,'/home/ubuntu/l4/lccyao2/pcflib/betteryao/evl',80,64,'/home/ubuntu/circuits/mod2dotproduct.pcf2','/home/ubuntu/inputs/evlgr.inp',ip,5588,'1'])


    alice.stdout.on('data', function (data) {
                //console.log('====== ' + data);
                var sf = '===' + data;
                io.sockets.emit('news', { f: sf } ); 
    });

    alice.on('close', function (code) {
                if (code !== 0) {
                console.log('alice process exited with code ' + code);
                }
    });
});

app.post('/danrun', function(req, res) {
	var ip = req.body.ip;
	var inpp = req.body.inpp;
	console.log("======run bob=======" + inpp);
	console.log("     " + ip);
	io.sockets.emit('news', { f: 'Starting Dan with Alice@ '+ip+' on input ' + inpp } ); 

	//bob = spawn('/home/ubuntu/build/gen', ['../circuits/mod2dotproduct.pcf2','../inputs/'+inpp+'.inp',ip,5001,'gg.out'])
	bob = spawn('mpirun', ['-n',16, '/home/ubuntu/l4/lccyao2/pcflib/betteryao/gen',80,64,'/home/ubuntu/circuits/mod2dotproduct.pcf2','/home/ubuntu/inputs/evlgr.inp',ip,5588,'1'])

	//mpirun -n 16 ./evl 80 64 ../../../../circuits/mod2dotproduct.pcf2 ../../../../inputs/evlgr.inp 127.0.0.1 5588 1


    bob.stdout.on('data', function (data) {
                var sf = '===' + data;
                io.sockets.emit('news', { f: sf } ); 
    });

    bob.on('close', function (code) {
                if (code !== 0) {
                console.log('alice process exited with code ' + code);
                }
    });
});


server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


io.sockets.on('connection', function (socket) {
        console.log(" ==== making connection ");
});



